# Idea

The idea is, we can install an IoT network contain nothing but some microphones and transmiters in the woods. The mics should record everything then transmit the audio back to server to analyze. If the server detects the sound of a working chainsaw, it would fire an alarm and report to the authority.

# Approaching

I've collected ~3000 samples of audio files, preprocessed (spliting, adding noises, standardizing) and separated them to 2 caregories to classify. Using **pyaudio** to convert theses audio files into MFCC data (which can be viewed as images, just for eye-checking) to greatly reduce the size of the training set without altering the data's integrity and properties, then build a model using **tensorflow keras**'s Sequential model with some Conv1D layers.